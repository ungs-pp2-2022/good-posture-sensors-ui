package good.posture.sensors.ui;

import good.posture.sensors.core.BodyPostureCore;
import good.posture.sensors.core.BodyPostureInitializer;
import good.posture.sensors.ui.controller.GoodPostureSensorsController;
import good.posture.sensors.ui.view.GoodPostureSensorsView;

public class Main {

	public static void main(String[] args) {
		
		BodyPostureInitializer initialize = new BodyPostureInitializer(args);
		BodyPostureCore core = initialize.initialize();
		GoodPostureSensorsView view = new GoodPostureSensorsView();
		GoodPostureSensorsController controller = new GoodPostureSensorsController(core, view);
		controller.initialize();
		view.show();
	}

}
