package good.posture.sensors.ui.controller;

import good.posture.sensors.core.BodyPostureCore;
import good.posture.sensors.ui.view.GoodPostureSensorsView;

public class GoodPostureSensorsController {

	private GoodPostureSensorsView goodPostureSensorsView;
	private BodyPostureCore bodyPostureCore;

	public GoodPostureSensorsController(BodyPostureCore bodyPostureCore,
			GoodPostureSensorsView goodPostureSensorsView) {
		this.bodyPostureCore = bodyPostureCore;
		this.goodPostureSensorsView = goodPostureSensorsView;
	}
	
	public void initialize() {
		doGet();
	}
	
	private String getBodyPart(String sex) {
		if (sex == "MASCULINO") {
			return "FOOT_MALE";
		}
		else {
			return "FOOT_FEMALE";
		}
	}

	private void doGet() {
		goodPostureSensorsView.getBtnEvaluarPostura().addActionListener(e -> {
			Integer postureInput = Integer.parseInt(goodPostureSensorsView.getPostureInput().getValue().toString());
			String sex = goodPostureSensorsView.getSexComboBox().getSelectedItem().toString();
			String bodyPart = this.getBodyPart(sex);

			boolean postureStatus = bodyPostureCore.checkPosture(bodyPart, postureInput);
			System.out.println("La postura es correcta? ---> " + postureStatus);
		});
		
		goodPostureSensorsView.getBtnActivateSoundAlert().addActionListener(e ->{
			//bodyPostureCore.activateAlertChannel("sound")
			goodPostureSensorsView.getBtnActivateSoundAlert().setEnabled(false);
			goodPostureSensorsView.getBtnDeactivateSoundAlert().setEnabled(true);
			System.out.println("Alarma sonora activada");
		});
		

		goodPostureSensorsView.getBtnDeactivateSoundAlert().addActionListener(e ->{
			//bodyPostureCore.deactivateAlertChannel("sound")
			goodPostureSensorsView.getBtnDeactivateSoundAlert().setEnabled(false);
			goodPostureSensorsView.getBtnActivateSoundAlert().setEnabled(true);
			System.out.println("Alarma sonora desactivada");
		});
		
		
		goodPostureSensorsView.getBtnActivateTelegramNotification().addActionListener(e ->{
			//bodyPostureCore.activateAlertChannel("telegram")
			goodPostureSensorsView.getBtnActivateTelegramNotification().setEnabled(false);
			goodPostureSensorsView.getBtnDeactivateTelegramNotification().setEnabled(true);
			System.out.println("Notificacion por telegram activada");
		});
		

		goodPostureSensorsView.getBtnDeactivateTelegramNotification().addActionListener(e ->{
			//bodyPostureCore.deactivateAlertChannel("telegram")
			goodPostureSensorsView.getBtnDeactivateTelegramNotification().setEnabled(false);
			goodPostureSensorsView.getBtnActivateTelegramNotification().setEnabled(true);
			System.out.println("Notificacion por telegram desactivada");
		});		
	}
	
	public void setViewVisible() {
		this.goodPostureSensorsView.show();
	}
	
}
