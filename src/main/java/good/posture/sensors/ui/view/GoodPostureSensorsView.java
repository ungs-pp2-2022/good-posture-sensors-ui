package good.posture.sensors.ui.view;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.text.NumberFormat;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.text.NumberFormatter;
import javax.swing.JFormattedTextField;
import javax.swing.JComboBox;
import java.awt.SystemColor;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GoodPostureSensorsView {

	private JFrame frmGoodPostureSensors;
	private JButton btnEvaluarPostura;
	private JFormattedTextField postureInput;
	private JComboBox sexComboBox;
	private JButton btnActivateSoundAlert;
	private JButton btnDeactivateSoundAlert;
	private JButton btnActivateTelegramNotification; 
	private JButton btnDeactivateTelegramNotification;


	public GoodPostureSensorsView() {
		initialize();
	}
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public void init() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					initialize();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void show() {
		frmGoodPostureSensors.setVisible(true);
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGoodPostureSensors = new JFrame();
		frmGoodPostureSensors.setResizable(false);
		frmGoodPostureSensors.setTitle("Good Posture Sensors App");
		frmGoodPostureSensors.setBounds(100, 100, 724, 500);
		frmGoodPostureSensors.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmGoodPostureSensors.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(0, 139, 139));
		panel.setBounds(10, 11, 688, 439);
		frmGoodPostureSensors.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblTitle = new JLabel("Good Posture Sensors");
		lblTitle.setForeground(SystemColor.text);
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 24));
		lblTitle.setHorizontalAlignment(SwingConstants.CENTER);
		lblTitle.setBounds(159, 11, 394, 47);
		panel.add(lblTitle);
		
		btnEvaluarPostura = new JButton("Evaluar Postura");
		btnEvaluarPostura.setForeground(new Color(0, 120, 215));
		btnEvaluarPostura.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnEvaluarPostura.setBounds(60, 299, 139, 33);
		panel.add(btnEvaluarPostura);
		
		NumberFormat format = NumberFormat.getInstance();
	    NumberFormatter formatter = new NumberFormatter(format);
	    formatter.setValueClass(Integer.class);
	    formatter.setMinimum(0);
	    formatter.setMaximum(Integer.MAX_VALUE);
	    formatter.setAllowsInvalid(false);
	    
		postureInput = new JFormattedTextField(format);
		postureInput.setFont(new Font("Tahoma", Font.PLAIN, 15));
		postureInput.setText("0");
		postureInput.setBounds(60, 234, 139, 33);
		panel.add(postureInput);
		
		JLabel lblNewLabel = new JLabel("Valor de postura");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel.setForeground(SystemColor.text);
		lblNewLabel.setBounds(60, 209, 139, 19);
		panel.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Sexo");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setForeground(SystemColor.text);
		lblNewLabel_1.setBounds(60, 123, 139, 19);
		panel.add(lblNewLabel_1);
		
		sexComboBox = new JComboBox();
		sexComboBox.setModel(new DefaultComboBoxModel(new String[] {"MASCULINO", "FEMEMINO"}));
		sexComboBox.setFont(new Font("Tahoma", Font.PLAIN, 15));
		sexComboBox.setBounds(60, 148, 139, 33);
		panel.add(sexComboBox);
		
		JLabel lblNewLabel_2 = new JLabel("Evaluación de postura");
		lblNewLabel_2.setForeground(SystemColor.text);
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_2.setBounds(60, 69, 176, 43);
		panel.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Notificaciones");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_3.setForeground(SystemColor.text);
		lblNewLabel_3.setBounds(455, 75, 156, 30);
		panel.add(lblNewLabel_3);
		
		btnActivateSoundAlert = new JButton("Activar");
		btnActivateSoundAlert.setForeground(new Color(0, 128, 0));
		btnActivateSoundAlert.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnActivateSoundAlert.setBounds(455, 163, 126, 32);
		panel.add(btnActivateSoundAlert);
		
		btnDeactivateSoundAlert = new JButton("Desactivar");
		btnDeactivateSoundAlert.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnDeactivateSoundAlert.setForeground(new Color(255, 0, 0));
		btnDeactivateSoundAlert.setBounds(455, 203, 126, 33);
		panel.add(btnDeactivateSoundAlert);
		
		btnActivateTelegramNotification = new JButton("Activar");
		btnActivateTelegramNotification.setForeground(new Color(0, 128, 0));
		btnActivateTelegramNotification.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnActivateTelegramNotification.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnActivateTelegramNotification.setBounds(455, 299, 126, 33);
		panel.add(btnActivateTelegramNotification);
		
		btnDeactivateTelegramNotification = new JButton("Desactivar");
		btnDeactivateTelegramNotification.setForeground(new Color(255, 0, 0));
		btnDeactivateTelegramNotification.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnDeactivateTelegramNotification.setBounds(455, 343, 126, 33);
		panel.add(btnDeactivateTelegramNotification);
		
		JLabel lblNewLabel_4 = new JLabel("Alerta sonora");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_4.setForeground(SystemColor.text);
		lblNewLabel_4.setBounds(455, 123, 139, 30);
		panel.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Alerta Telegram");
		lblNewLabel_5.setForeground(SystemColor.text);
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_5.setBounds(455, 258, 139, 30);
		panel.add(lblNewLabel_5);
	}

	public JButton getBtnEvaluarPostura() {
		return btnEvaluarPostura;
	}

	public JFormattedTextField getPostureInput() {
		return postureInput;
	}

	public JComboBox getSexComboBox() {
		return sexComboBox;
	}

	public JButton getBtnActivateSoundAlert() {
		return btnActivateSoundAlert;
	}

	public JButton getBtnDeactivateSoundAlert() {
		return btnDeactivateSoundAlert;
	}

	public JButton getBtnActivateTelegramNotification() {
		return btnActivateTelegramNotification;
	}

	public JButton getBtnDeactivateTelegramNotification() {
		return btnDeactivateTelegramNotification;
	}	
}
